package com.zuitt.example;

import java.util.Scanner;
public class Activity {
    public static void main(String [] args){
        Scanner Person = new Scanner(System.in);
        System.out.println("First Name:");
        String firstName = Person.nextLine();
        System.out.println("Last Name:");
        String lastName = Person.nextLine();
        System.out.println("First Subject Grade:");
        double firstSubject = Person.nextInt();
        System.out.println("Second Subject Grade:");
        double secondSubject = Person.nextInt();
        System.out.println("Third Subject Grade:");
        double thirdSubject = Person.nextInt();
        System.out.println("Good day " + firstName + ". Your grade average is: " + ((firstSubject + secondSubject + thirdSubject) / 3));
    }
}
