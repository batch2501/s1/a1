package com.zuitt.example;

public class Variables {
    public static void main(String [] args){
        int age;
        char middleNAme;
        // Variable declaration vr Initialization
        int x;
        int y = 0;
        // initialization after declaration
        x = 1;

        // Output to the console
        System.out.println("The value of y is " + y + " and the value of x is " + x);

        int wholeNumber = 100;
        System.out.println(wholeNumber);

        // long
        // L is being added at the end of the long number to be recognized
        long worldPopulation = 765852365316L;
        System.out.println(worldPopulation);

        // float
        // add f at the end of the float to be recognized.
        float piFloat = 3.1415926359f;
        System.out.println(piFloat);

        // double - floating point values
        double piDouble = 3.1415926359f;
        System.out.println(piDouble);

        // char - single characters
        // uses single quote.
        char letter = 'a';
        System.out.println(letter);

        //boolean true or false
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        // constants
        // Java uses the "final" keyword so the variable's value cannot be changed.
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        // PRINCIPAL = 4000;

        // String
        // Stores a sequence or array of characters
        // Strings are actually objects that can use methods

        String username = "JSmith";
        System.out.println(username);

        // Sample string method
        int stringLength = username.length();
        System.out.println(stringLength);
    }
}
